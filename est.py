#instalar
#sudo python -m pip install unicodecsv

#ejecutar:
#python2 est.py

import re
import unicodecsv as csv
import pandas as pd
import math
from sets import Set
import matplotlib
import numpy as np
import matplotlib.pyplot as pl
import scipy as sc


###############################################################
#devuelve la entropia de un string
###############################################################
def entropy(row):
    stringList = list(row)
    alfabeto = list(Set(stringList)) # lista de los simbolos en string

    # calcula la frecuencia of de c/simbolo
    frecList = []
    for simbolo in alfabeto:
        cont = 0
        for sym in stringList:
            if simbolo == sym:
                cont += 1
        frecList.append(float(cont) / len(stringList))

    # calcula entropia final
    entropia = 0.0
    for frecuencia in frecList:
        entropia = entropia + frecuencia * math.log(frecuencia, 2)
    entropia = -entropia

    return round(entropia, 2)

###############################################################
#Leer el csv y escribir un nuevo csv con los datos agregados
###############################################################

reader = csv.reader(open("comentarios_por_pais.csv", "r"),  delimiter=';', encoding='utf-8')
writer = csv.writer(open("datos_agregados.csv", "w"),  delimiter=';', encoding='utf-8')

id_counter = 0
for row in reader:
    if id_counter == 0:
        #agregar nueva columna (el nombre)
        row.append(u'longitud')
        row.append(u'entropia')
        id_counter += 1
    else:
        cant_caracteres=len(row[1])
        entropia=entropy(row[1])
        row.append(cant_caracteres)
        row.append(entropia)
    #escribir la longitud en otro archivo
    writer.writerow(row)


###############################################################
#Abrir el nuevo csv y hacer el grafico estadistico de la longitud
###############################################################



df = pd.read_csv("datos_agregados.csv", sep=';', na_values='.')

#grafico de longitud y entropia por pais
#el boxplot proporciona datos estadisticos como la mediana y los valores maximo y minimo de la longitud
bp = df.boxplot(column='longitud', by='pais')
axes = pl.gca()
axes.set_ylim([0,800])

bp2 = df.boxplot(column='entropia', by='pais')
axes = pl.gca()
axes.set_ylim([0,5.5])
pl.show()