#!/bin/bash
chmod 744 supermenu.sh
#------------------------------------------------------
# PALETA DE COLORES
#------------------------------------------------------
#setaf para color de letras/setab: color de fondo
    red=`tput setaf 1`;
    green=`tput setaf 2`;
    blue=`tput setaf 4`;
    bg_blue=`tput setab 4`;
    reset=`tput sgr0`;
    bold=`tput setaf bold`;
#------------------------------------------------------
# VARIABLES GLOBALES
#------------------------------------------------------
proyectoActual="Trabajo Práctico Nº2 Sistemas Operativos y Redes";
proyectos="/home/alumno/tp2_sor1_yasutake";

#------------------------------------------------------
# DISPLAY MENU
#------------------------------------------------------
imprimir_menu () 
{
    imprimir_encabezado "\t  S  U  P  E  R  -  M  E  N U ";
    
    echo -e "\t\t El proyecto actual es:";
    echo -e "\t\t $proyectoActual";
    
    echo -e "\t\t";
    echo -e "\t\t Opciones:";
    echo "";
    echo -e "\t\t\t a.  Calcular entropia - Boxplot";
    echo -e "\t\t\t b.  Dirección IP propia y de red";
    echo -e "\t\t\t c.  Dirección del router";
    echo -e "\t\t\t d.  Dispositivos conectados";        
    echo -e "\t\t\t e.  Realizar Traceroute";
    echo -e "\t\t\t f.  Interfaces de red del host";
    echo -e "\t\t\t g.  Más opciones...";               
    echo -e "\t\t\t q.  Salir";
    echo "";
    echo -e "Escriba la opción y presione ENTER";
}

imprimir_menu2 () 
{
    imprimir_encabezado "\t  S  U  P  E  R  -  M  E  N U ";
    
    echo -e "\t\t El proyecto actual es:";
    echo -e "\t\t $proyectoActual";
    
    echo -e "\t\t";
    echo -e "\t\t Más opciones:";
    echo "";
    echo "";
    echo -e "\t\t\t h. Puertos abiertos para TCP: 1-32000 propios y remoto";
    echo -e "\t\t\t i. Loguearse a computadora remota";
    echo -e "\t\t\t j. Copiado remoto SCP y FTP";
    echo -e "\t\t\t k. tcdump menu";
    echo -e "\t\t\t l. Ver Créditos";
    echo -e "\t\t\t q.  Salir";
    echo "";
    echo -e "Escriba la opción y presione ENTER";
}

imprimir_menu3 () 
{
    imprimir_encabezado "\t  S  U  P  E  R  -  M  E  N U ";
    
    echo -e "\t\t El proyecto actual es:";
    echo -e "\t\t $proyectoActual";
    
    echo -e "\t\t";
    echo -e "\t\t tcpdump menu:";
    echo "";
    echo "";
    echo -e "\t\t\t m. Capturar paquetes de alguna interfaz";
    echo -e "\t\t\t n. Capturar paquetes y guardardo en un archivo";
    echo -e "\t\t\t o. Ver en pantalla los paquetes guardados";
    echo -e "\t\t\t p. Capturar paquetes de una dirección origen específica";
    echo -e "\t\t\t q. Capturar paquetes de una dirección destino especifica";
    echo -e "\t\t\t r.  Salir";
    echo "";
    echo -e "Escriba la opción y presione ENTER";
}

seleccionar_menu2 () 
{
    while  true
    do
        # 1. mostrar el menu
        imprimir_menu2;
        # 2. leer la opcion del usuario
        read opcion;
        
        case $opcion in
            h|H) h_funcion;;
            i|I) i_funcion;;
            j|J) j_funcion;;
            k|K) k_funcion;;
            l|L) l_funcion;;
            q|Q) break;;
            *) malaEleccion;;
        esac
        esperar;
    done
}

seleccionar_menu3 () 
{
    while  true
    do
        # 1. mostrar el menu
        imprimir_menu3;
        # 2. leer la opcion del usuario
        read opcion;
        
        case $opcion in
            m|M) m_funcion;;
            n|N) n_funcion;;
            o|O) o_funcion;;
            p|P) p_funcion;;
            q|Q) q_funcion;;
            r|R) break;;
            *) malaEleccion;;
        esac
        esperar;
    done
}

#------------------------------------------------------
# FUNCTIONES AUXILIARES
#------------------------------------------------------

imprimir_encabezado () 
{
    clear;
    #Se le agrega formato a la fecha que muestra
    #Se agrega variable $USER para ver que usuario está ejecutando
    echo -e "`date +"%d-%m-%Y %T" `\t\t\t\t\t USERNAME:$USER";
    echo "";
    #Se agregan colores a encabezado
    echo -e "\t\t ${bg_blue} ${red} ${bold}--------------------------------------\t${reset}";
    echo -e "\t\t ${bold}${bg_blue}${red}$1\t\t${reset}";
    echo -e "\t\t ${bg_blue}${red} ${bold} --------------------------------------\t${reset}";
    echo "";
}

esperar () 
{
    echo "";
    echo -e "Presione enter para continuar";
    read ENTER ;
}

malaEleccion () 
{
    echo -e "Selección Inválida ..." ;
}

decidir () 
{
    echo $1;
    while true; 
    do
        echo "desea ejecutar? (s/n)";
        read respuesta;
        case $respuesta in
            [Nn]* ) break;;
            [Ss]* ) eval $1
            break;;
            * ) echo "Por favor tipear S/s ó N/n.";;
        esac
        esperar;
    done
}

lanzarCreditos() 
{
    echo "";
    echo "Sistemas Operativos y Redes: Trabajo Práctico 2";
    echo "";
    echo "Alumno: Leandro Yasutake";
    echo "";
    echo "Docentes: Andrés Rojas Paredes - Hvara Ocar";
    echo "";
}

#------------------------------------------------------
# EJERCICIOS
#------------------------------------------------------

calcularEntropiaBoxplot() 
{
    echo "Calculando entropia y realizando boxplot...";
    python est.py;
}

mostrarIP() 
{
    echo "Su dirección de IP es:";
    ifconfig | grep -i "inet" -m 1|awk '{print $2}';
    echo "";
    echo "Su dirección de red es:";
    hostname --ip-address;
}

mostrarIProuter() 
{
    echo "La dirección del router es:";
    ip route show | grep -i 'default via'| awk '{print $3 }';
}

mostrarDispositivosConectados() 
{
    echo "Dispositivos conectados en la red:";
    direccionBusqueda=$(ip route show | awk 'N=1 {print $N}' | grep /);
    sudo nmap -sP $direccionBusqueda;
    printf "\nVerificar conectividad de ip: "
    read ip;
    ping -w 5 $ip;
}

mostrarInterfaces() 
{
    echo "Interfaces de red del host:";
    ifconfig | cut -d ' ' -f1| tr ':' '\n' | awk NF;
}

mostrarPuertosAbiertos() 
{

    echo "Puertos abiertos para TCP: 1-32000:";
    netcat -zv 127.0.0.1 1-32000 2>&1 | grep succeeded;

    echo "Puertos abiertos para TCP: 1-32000 de host remoto: 192.168.1.14";
    ssh  grupo_12@192.168.1.14;
    netcat -zv 192.168.1.14 1-32000 2>&1 | grep succeeded;
}

realizarTraceroute()
{
    echo "Enviando paquetes a www.u-tokyo.ac.jp...";
    sudo /home/lilo/local/inetutils-1.9.4/src/traceroute --wait 1 --resolve-hostnames -I www.u-tokyo.ac.jp;
}

logRemoto() 
{
	echo "Conexión a host remoto: grupo_12@192.168.1.14";
	echo ""
	ssh -X grupo_12@192.168.1.14;
    
    #read -p "Ingrese el puerto: " puerto;
	#read -p "Ingrese el usuario: " usuario;
	#read -p "Ingrese la ip del servidor: " ip;
	#echo ""
    #echo "Intentando conexión...";
	#ssh -X -p $puerto $usuario@$ip;
}

copiadoRemoto() 
{
	echo "Copiado de Datos a host remoto root@192.168.1.14: SCP"
	echo "Intentando copiado remoto servidor.c";
    scp /home/leo/tp2_sor1_yasutake/servidor.c  grupo_12@192.168.1.14:/home;
    echo " ";
    echo "Copiado de Datos a host remoto ftp.cs.brown.edu: FTP"
    echo "Abriendo puertos necesarios...";
    set -x;
    sudo modprobe ip_conntrack;
    sudo modprobe ip_conntrack_ftp;
    sudo iptables -A INPUT -p tcp -s 0/0 --sport 1024:65535 -d 202.54.1.20 --dport 21 -m state --state NEW,ESTABLISHED -j ACCEPT;
    sudo iptables -A OUTPUT -p tcp -s 202.54.1.20 --sport 21 -d 0/0 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT;
    sudo iptables -A INPUT -p tcp -s 0/0 --sport 1024:65535 -d 202.54.1.20 --dport 1024:65535 -m state --state ESTABLISHED,RELATED -j ACCEPT;
    sudo iptables -A OUTPUT -p tcp -s 202.54.1.20 --sport 1024:65535 -d 0/0 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT;
    sudo iptables -A OUTPUT -p tcp -s 202.54.1.20 --sport 20 -d 0/0 --dport 1024:65535 -m state --state ESTABLISHED,RELATED -j ACCEPT;
    sudo iptables -A INPUT -p tcp -s 0/0 --sport 1024:65535 -d 202.54.1.20 --dport 20 -m state --state ESTABLISHED -j ACCEPT;
    set +x;
    echo "Intentando conexión";
    echo "tipear:";
    echo "usuario: anonymous - pass: """;
    echo "passive";
    echo "cd incoming";
    echo "put sniffer.c";
    ftp ftp.cs.brown.edu;
    
	
    #read -p "Ingrese el path y nombre del archivo: " archivo;
	#read -p "Ingrese el path remoto donde desea guardar el archivo: " ubicacion;
	#read -p "Ingrese el usuario remoto: " usuario;
	#read -p "Ingrese la direccion ip del servidor remoto: " ip;
	#echo "";
    #echo "Intentando copiado remoto...";
	#scp $usuario@$ip:$archivo $ubicacion;

    #servidor - host
	#scp $usuario@$ip:$archivo $ubicacion 
    
    #host - servidormostrarPuertosAbiertos() 
{

    echo "Puertos abiertos para TCP: 1-32000:";
    netcat -zv 127.0.0.1 1-32000 2>&1 | grep succeeded;

    echo "Puertos abiertos para TCP: 1-32000 de host remoto: 192.168.1.14";
    ssh  grupo_12@192.168.1.14;
    netcat -zv 192.168.1.14 1-32000 2>&1 | grep succeeded;
}
	#scp $archivo $usuario@ip:$ubicacion                  
}

capturarPaquetes() 
{
	echo "Capturando paquetes de la interfaz local...";
    sudo tcpdump -i lo;
}

capturarGuardarPaquetes() 
{
	echo "Captura y guardado de paquetes  de la interfaz local...";
    sudo tcpdump -w 0001.pcap -i lo;
}

leerPaquetes() 
{
	echo "Abriendo archivo de captura de paquetes...";
    sudo tcpdump -r 0001.pcap;
}

capturarOrigen() 
{
	echo "Ingrese los datos para la captura de paquetes de una dirección origen específica:";
    echo "";

    #read -p "Ingrese la dirección de origen: " $origen;
	#read -p "Ingrese la interfaz de red deseada: " $interfaz;
	#echo ""
    #sudo tcpdump -i $interfaz src $origen;

    echo "Intentando captura;...";
    sudo tcpdump -i enp0s3 src 192.168.1.14;
}

capturarDestino() 
{
	echo "Ingrese los datos para la captura de paquetes de una dirección destino especifica:";
    echo "";
    
    #read -p "Ingrese la dirección de destino: " $destino;
	#read -p "Ingrese la interfaz de red deseada: " $interfaz;
	#echo ""
    #sudo tcpdump -i $interfaz dst $destino;

    echo "Intentando captura;...";
    sudo tcpdump -i enp0s3 dst 192.168.1.14;
}

#------------------------------------------------------
# FUNCTIONES del MENU
#------------------------------------------------------
a_funcion () 
{
    imprimir_encabezado "\tOpción a";
    calcularEntropiaBoxplot;
}

b_funcion () 
{
    imprimir_encabezado "\tOpción b";
    mostrarIP;
}

c_funcion () 
{
    imprimir_encabezado "\tOpción c";
    mostrarIProuter;     
}

d_funcion () 
{
    imprimir_encabezado "\tOpción d";
    mostrarDispositivosConectados;
}

e_funcion () 
{
    imprimir_encabezado "\tOpción e";        
    realizarTraceroute;
}

f_funcion () 
{
    imprimir_encabezado "\tOpción f";
    mostrarInterfaces;      
}

g_funcion () 
{
     imprimir_encabezado "\tOpción g";
    seleccionar_menu2;
}

h_funcion () 
{
    imprimir_encabezado "\tOpción h";
    mostrarPuertosAbiertos;
}

i_funcion () 
{
    imprimir_encabezado "\tOpción i";
    logRemoto;
}

j_funcion () 
{
    imprimir_encabezado "\tOpción j";
    copiadoRemoto;
}

k_funcion () 
{
    imprimir_encabezado "\tOpción k";
    seleccionar_menu3;
}

l_funcion () 
{
    imprimir_encabezado "\tOpción l";
    lanzarCreditos;
}

m_funcion () 
{
    imprimir_encabezado "\tOpción m";
    capturarPaquetes;
}

n_funcion () 
{
    imprimir_encabezado "\tOpción n";
    capturarGuardarPaquetes;
}

o_funcion () 
{
    imprimir_encabezado "\tOpción o";
    leerPaquetes;
}

p_funcion () 
{
    imprimir_encabezado "\tOpción p";
    capturarOrigen;
}

q_funcion () 
{
    imprimir_encabezado "\tOpción q";
    capturarDestino;     
}

#------------------------------------------------------
# LOGICA PRINCIPAL
#------------------------------------------------------
while  true
do
    # 1. mostrar el menu
    imprimir_menu;
    # 2. leer la opcion del usuario
    read opcion;
    
    case $opcion in
        a|A) a_funcion;;
        b|B) b_funcion;;
        c|C) c_funcion;;
        d|D) d_funcion;;
        e|E) e_funcion;;
        f|F) f_funcion;;
        g|G) g_funcion;;
        q|Q) break;;
        *) malaEleccion;;
    esac
    esperar;
done
